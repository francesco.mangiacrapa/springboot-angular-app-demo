import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


import { MyFirstCAngularComponent } from './my-first-c-angular/my-first-c-angular.component';

const routes: Routes = [
  //{path: '', component: HelloWorldComponent},
  {path: '', component: MyFirstCAngularComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
