import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MyFirstAngularGreetingModelClass } from '../model/my-first-angular-greeting-model-class';

@Injectable({
  providedIn: 'root'
})
export class MyFirstSAngularService {

  constructor(private http: HttpClient) { }

  executeMyFirstHttpCall() {
    return this.http.get<MyFirstAngularGreetingModelClass>('http://localhost:8080/api/greeting-obj');
  }
}
