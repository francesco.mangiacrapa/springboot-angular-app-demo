/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MyFirstSAngularService } from './my-first-s-angular.service';

describe('Service: MyFirstSAngular', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MyFirstSAngularService]
    });
  });

  it('should ...', inject([MyFirstSAngularService], (service: MyFirstSAngularService) => {
    expect(service).toBeTruthy();
  }));
});
