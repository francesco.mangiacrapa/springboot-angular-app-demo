import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MyFirstCAngularComponent } from './my-first-c-angular/my-first-c-angular.component';
import { HttpClientModule } from '@angular/common/http';
import { MyFirstSAngularService } from './service/my-first-s-angular.service';

@NgModule({
  declarations: [
     AppComponent,
     MyFirstCAngularComponent
  ],
  imports: [
     BrowserModule,
     AppRoutingModule,
     HttpClientModule
  ],
  providers: [
   MyFirstSAngularService
  ],
  bootstrap: [
     AppComponent
  ]
})
export class AppModule { }
