import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { MyFirstSAngularService } from '../service/my-first-s-angular.service';

@Component({
  selector: 'app-my-first-c-angular',
  templateUrl: './my-first-c-angular.component.html',
  styleUrls: ['./my-first-c-angular.component.css']
})
export class MyFirstCAngularComponent implements OnInit {

  welcomeMessage = '';

  constructor(private myFirstAnguluarService: MyFirstSAngularService) { }

  ngOnInit() {
    this.myFirstAnguluarService.executeMyFirstHttpCall().subscribe((res) => {
      this.welcomeMessage = res.content;
    });
  }

}
