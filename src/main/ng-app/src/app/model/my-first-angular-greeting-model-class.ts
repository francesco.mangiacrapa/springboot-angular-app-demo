/*
* This class is a model for mapping the Greeting.java
* provided by Spring
*/
export class MyFirstAngularGreetingModelClass {
  id: number;
  content: string;
  constructor(private _id: number, public message: string) {
    this.id = _id;
    this.content = message;
  }
}
