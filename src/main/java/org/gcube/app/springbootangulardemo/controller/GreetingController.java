package org.gcube.app.springbootangulardemo.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/api")
//@ResponseBody
public class GreetingController {

	//This return the templates/greeting.html
	@RequestMapping(value = "/greeting", method = RequestMethod.GET, produces = {"text/html"})
	public String greeting(@RequestParam(name="name", required=false, defaultValue="World") final String name,
            final Model model) {

		//This return the greeting.html
		model.addAttribute("name", name);
		return "greeting";
	}
}