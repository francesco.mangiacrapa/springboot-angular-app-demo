package org.gcube.app.springbootangulardemo.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.gcube.app.springbootangulardemo.model.Greeting;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api")
public class GreetingRestController {

    String template = "Hello, %s!";
	AtomicLong counter = new AtomicLong();

    //Return the class Greeting
	@RequestMapping("/greeting-obj")
    public Greeting greeting(@RequestParam(value = "name", defaultValue = "World") String name) {

         return new Greeting(counter.incrementAndGet(), String.format(template, name));
	}

}