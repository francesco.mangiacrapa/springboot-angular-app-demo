package org.gcube.app.springbootangulardemo.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomeController {

     @RequestMapping("/")
     public String home(){
         return "Hello, I'm the REST "+HomeController.class.getSimpleName();
     }
 }