package org.gcube.app.springbootangulardemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootAngularAppDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootAngularAppDemoApplication.class, args);
		System.out.println(SpringbootAngularAppDemoApplication.class.getSimpleName()+ " called");
	}

}
