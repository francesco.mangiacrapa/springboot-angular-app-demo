package org.gcube.app.springbootangulardemo.servlet;

import org.gcube.app.springbootangulardemo.SpringbootAngularAppDemoApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

public class ServletInitializer extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		System.out.println(this.getClass().getSimpleName()+ " configure called");
		return application.sources(SpringbootAngularAppDemoApplication.class);
	
	}

}